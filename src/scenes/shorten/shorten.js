import {Component} from "react";
import React from "react";
import './shorten.css'
import TextField from "@material-ui/core/TextField";
import Typography from '@material-ui/core/Typography';
import InputAdornment from "@material-ui/core/InputAdornment";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import ClipBoardLink from "./ClipBoardLink";
import Logo from "./../../components/Logo";

class Shorten extends Component {
    constructor() {
        super();
        this.state = {
            humanVerification: false,
            passwordProtected: false,
            shorten: "https://",
            to: "https://joseflorez.co/"
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.name === ('humanVerification' || 'passwordProtected') ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
        this.setState({
            to: "https://joseflorez.co/" + Math.random().toString(36).substring(7)
        })

    }

    render() {
        return (
            <div className="Shorten">
                <div className="contentShorten">
                    <div className="labelCard shortenLabel">
                        <Logo/>
                    </div>

                    <form className="formShorten" noValidate autoComplete="off">
                        <Typography variant="h4" gutterBottom>
                            Shorten
                        </Typography>
                        <FormGroup>
                            <TextField
                                id="outlined-name"
                                label="Shorten"
                                type="url"
                                name="shorten"
                                className="form-input-shorten"
                                value={this.state.shorten}
                                onChange={this.handleInputChange}
                                margin="normal"
                                variant="outlined"
                            />
                        </FormGroup>
                        <TextField
                            id="outlined-adornment-password"
                            className="form-input-shorten"
                            variant="outlined"
                            type={'text'}
                            label="To"
                            name="to"
                            disabled={true}
                            value={this.state.to}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <ClipBoardLink text={this.state.to} />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox value="1" color={"primary"} />}
                                label="Require human verification"
                                name="humanVerification"
                                checked={this.state.humanVerification}
                                onChange={this.handleInputChange}
                            />

                            <FormControlLabel
                                control={<Checkbox value="1" color={"primary"} />}
                                label="Password protect this link"
                                name="passwordProtected"
                                checked={this.state.passwordProtected}
                                onChange={this.handleInputChange}
                            />
                        </FormGroup>

                        <div className="actionFormShorten">
                            <Button variant="outlined" className="margin-top-15" color="primary">
                                Shorten
                            </Button>

                            <Typography variant="subtitle2" className="margin-top-30" gutterBottom>
                                Do you already have an account? <Link to="/">Login</Link>
                            </Typography>
                        </div>

                    </form>
                </div>
            </div>
        )
    }
}

export default Shorten;