import React from 'react';
import PropTypes from 'prop-types';
import Clipboard from 'react-clipboard.js';
import FileCopy from "@material-ui/icons/FileCopy";
import IconButton from "@material-ui/core/IconButton";


class ClipBoardLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.onSuccess = this.onSuccess.bind(this);
    }

    onSuccess(e) {
        alert(e.text)
    }

    render() {
        const {text} = this.props;

        return (
            <Clipboard
                data-clipboard-text={text}
                onSuccess={this.onSuccess}
                component="i"
            >
                <IconButton
                    aria-label="Toggle password visibility"
                >
                    <FileCopy/>
                </IconButton>
            </Clipboard>
        );
    }
}

ClipBoardLink.propTypes = {
    text: PropTypes.string.isRequired,
};

export default ClipBoardLink;
